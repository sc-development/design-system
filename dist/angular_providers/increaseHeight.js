function IncreaseHeight($window) {
  return {
    restrict: 'C',
    link: function(scope, element, attrs) {
      element.css('height', window.innerHeight - 69);
      $(window).resize(function() {
        element.css('height', window.innerHeight - 69);
      });
    }
  };
}
