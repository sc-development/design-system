var NDLTwo = Class.create();
NDLTwo.prototype = {
  initialize: function(request, response) {
    this.json = new JSON();
    this.request = request;
    this.response = response;
    this.error = new sn_ws_err.ServiceError();
  },

  log: function(message) {
    gs.log(message, this.type);
  },

  getRecords: function(table, fields, query, sysID) {
    function getRecord(rec, fieldArray) {
      var recordInfo = {};
      for (var i = 0; i < fieldArray.length; i++) {
        var field = fieldArray[i].toString().trim();
        recordInfo[field.trim()] = rec.getDisplayValue(field);
        if (!recordInfo[field.trim()]) {
          recordInfo[field.trim()] = rec.getValue(field);
        }
      }
      return recordInfo;
    }

    var records = [];
    var fieldArray = [];
    var isGet = false;
    table = table || this.getParamAsString('table');
    fields = fields || this.getParamAsString('fields');
    query = query || this.getParamAsString('query');
    sysID = sysID || this.getParamAsString('sys_id');

    if (fields.toString() != '') {
      fieldArray = fields.split(',');
    }
    if (
      table.toString() == 'solutions' ||
      table.toString() == 'solution_artifacts' ||
      table.toString() == 'u_widget_pages' ||
      table.toString() == 'u_page_components' ||
      table.toString() == 'sys_attachment' ||
      table.toString() == 'kb_knowledge'
    ) {
      var rec = new GlideRecord(table);
      if (sysID.toString() == '' || !sysID) {
        rec.addEncodedQuery(query);
        rec.query();
        var totalRecords = rec.getRowCount();
      } else {
        isGet = true;
        rec.get(sysID);
      }
      if (!isGet) {
        while (rec.next()) {
          records.push(getRecord(rec, fieldArray));
        }
      } else {
        return getRecord(rec, fieldArray);
      }
      return records;
    }
  },

  getParamAsString: function(paramName) {
    if (this.request.queryParams.hasOwnProperty(paramName)) {
      return this.request.queryParams[paramName] + '';
    }
    return false;
  },
  type: 'NDLTwo'
};