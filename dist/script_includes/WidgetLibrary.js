var WidgetLibrary = Class.create();
WidgetLibrary.prototype = {
	
	initialize: function(request, response) {
		this.json = new JSON();
		this.request = request;
		this.response = response;
		this.error = new sn_ws_err.ServiceError();
	},
	
	log: function(message){
		gs.log(message, this.type);
	},
	
	getPages: function(solutionID){
		var experience = {};
		var pages = [];
		solutionID = solutionID || this.getParamAsString('solution_id');
		var sol = new GlideRecord('solutions');
		sol.addEncodedQuery('publishing_goal=Experience');
		sol.get(solutionID);
		experience.name = sol.getValue('name');
		var page = new GlideRecord('u_widget_pages');
		page.addQuery('u_experience', solutionID);
		page.orderBy('u_order');
		page.query();
		while (page.next()) {
			var components = [];
			var comp = new GlideRecord('u_page_components');
			comp.addEncodedQuery('u_widget_page='+page.getUniqueValue());
			comp.orderBy('u_order');
			comp.query();	
			while (comp.next()) {
				var compInfo = {
					type: comp.getDisplayValue('u_type'),
					screenshot: comp.u_screenshot.getDisplayValue(),
					solutionWidget: comp.getDisplayValue('u_solution_widget'),
					solutionWidgetSysId: comp.getValue('u_solution_widget'),
					hotspotName: comp.getValue('u_hotspot_name'),
					hotspotColor: comp.getValue('u_hotspot_color'),
					showHotspot: comp.getValue('u_show_hotspot'),
					order: comp.getValue('u_order')
				};
				//sush adding this to get screenshot
				if(!compInfo.screenshot && compInfo.type == 'Solution/Widget'){
					//compInfo.screenshot = comp.u_solution_widget.u_widget_screenshot.getDisplayValue();
					var sa = new GlideRecord('solution_artifacts');
					sa.addQuery('solution', comp.u_solution_widget);
					sa.addQuery('type', 'Primary Icon');
					sa.query();
					
					if (sa.next()) {
						compInfo.screenshot = sa.screenshot.getDisplayValue();
					}
				}
				components.push(compInfo);
			}
			var pageInfo = {
				name: page.getValue('u_page_name'), 
				components: components
			};
			pages.push(pageInfo);
			experience.pages = pages;
		}
		return experience;
	},
				
				
	getParamAsString: function(paramName) {
		if (this.request.queryParams.hasOwnProperty(paramName)) {
			return this.request.queryParams[paramName] + '';
		}
		return false;
	},
				
	type: 'WidgetLibrary'
};