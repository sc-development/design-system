(function(){

	var menu_id = $sp.getValue('sp_rectangle_menu'); // instance sys_id

	var grMenu = new GlideRecord('sp_rectangle_menu_item');
	data.menu_id = menu_id;

	grMenu.addEncodedQuery('active=true^sp_rectangle_menu='+ menu_id + '^ORDERBYorder');
	grMenu.query();
	data.menu = [];

	while(grMenu.next()){
		var menuItem = {};
		menuItem.sys_id = grMenu.sys_id.toString();
		menuItem.name = grMenu.sys_name.toString();
		menuItem.label = grMenu.label.toString();
		menuItem.type = grMenu.type.toString();
		menuItem.url = grMenu.url.toString();
		menuItem.url_target = grMenu.url_target.toString();
		menuItem.condition = grMenu.condition.toString();
		menuItem.sp_page = grMenu.sp_page.toString();

		if(menuItem.sp_page){
			var grPage = new GlideRecord('sp_page');
			grPage.get(menuItem.sp_page);
			menuItem.page_id = grPage.id.toString();
		}

		data.menu.push(menuItem);
	}

	data.login_page = $sp.getValue('login_page');
	
	data.feedbackWidget = $sp.getWidget('ndl-simple-feedback');
	data.subpageWidget = $sp.getWidget('ndl-sub-header');
	
	data.isLoggedIn = GlideSession.get().isLoggedIn();

})();