(function (angular) {
    angular.module('ndl-frontend', [])
    // .config(function () {
    // })
        .run(function ($rootScope, $location, $window, $timeout, $http, spUtil, glideUserSession, snRecordWatcher) {
            $scope = {};
            // Loop thru empting this out
            if ($rootScope.$$listenerCount["$locationChangeSuccess"] > 0) {
                $rootScope.$$listeners["$locationChangeSuccess"][0] = null;
            }


            var oid = $location.search().id;
            var oldPath = $location.path();


            function isPublicOrUserLoggedIn(page, user) {
                if (page.public)
                    return true;

                if (user.user_name == "guest")
                    return false;

                return true;
            }

            function getUrl() {
                var currentParms = $location.search();
                var params = {};
                angular.extend(params, currentParms);
                params.time = new Date().getTime();
                params.request_uri = $location.url();
                return '/api/now/sp/page?' + $.param(params);
            }

            function refreshPage(dataURL) {
                debugger
                if (window.pageData && !pageLoaded) {
                    pageLoaded = true;
                    loadPage(pageData);
                } else {
                    $http.get(dataURL, {headers: spUtil.getHeaders()}).success(function (response) {
                        loadPage(response.result);
                    });
                }
            }

            function loadPage(response) {

                $scope.firstPage = false;
                $rootScope.containers = _.filter(response.containers, {'subheader': false});
                $rootScope.subheaders = _.filter(response.containers, {'subheader': true});
                $rootScope.rectangles = response.rectangles;
                $rootScope.style = '';
                // promote to root
                var p = response.page;
                var u = response.user;

                /*
                 * All initial page loads already handle authentication (showing login page)
                 * so if $location has changed and page is not public, reload to show the login page.
                 * That way, the URL stays the same, and user will see correct page after login.
                 */
                if (!isPublicOrUserLoggedIn(p, u)) {
                    if ($scope.locationChanged) {
                        $window.location.href = $location.absUrl();
                        return;
                    }
                }

                $rootScope.page = $scope.page = p;
                setCSS(p);
                if (response.portal.title)
                    $window.document.title = (p.title) ? response.portal.title + ' - ' + p.title : response.portal.title;
                else
                    $window.document.title = p.title;

                $timeout(function () {
                    jQuery('section, .flex-item').scrollTop(0);
                });

                $rootScope.theme = $scope.theme = response.theme;
                var style = "";
                if ($scope.isNative)
                    style = 'isNative';

                response.portal.logoutUrl = "/logout.do?sysparm_goto_url=/" + response.portal.url_suffix;
                $rootScope.portal = $scope.portal = response.portal;
                if (!$scope.user) {
                    $rootScope.user = $scope.user = {};
                    glideUserSession.loadCurrentUser().then(function (g_user) {
                        $rootScope.g_user = g_user;
                    });
                }

                angular.extend($scope.user, response.user);
                $scope.user.logged_in = $scope.user.user_name != 'guest';

                $rootScope.$broadcast('$$uiNotification', response.$$uiNotification);
                snRecordWatcher.init();
            }


            function setCSS(page) {
                // remove current page css
                //jQuery('#sp_page_css').remove();  // page specific css
                jQuery("style[data-page-id='" + page.sys_id + "']").remove();
                // add new page specific css if it exists
                if (page.css) {
                    var buf = [
                        '<style type="text/css" data-page-id="' + page.sys_id + '" data-page-title="' + page.title + '">',
                        page.css,
                        '</style>'
                    ];

                    jQuery(jQuery(buf.join('\n'))).appendTo('head');
                }
            }

            $rootScope.$on('$locationChangeSuccess', function (e, newUrl, oldUrl) {
                $rootScope.$broadcast("$$uiNotification.dismiss");
                var locationChanged = (oldUrl != newUrl);
                var s = $location.search();
                var p = $location.path();

                // Has portal changed?
                if (oldPath != p) {
                    $window.location.href = $location.absUrl();
                    return;
                }

                // page is handling?
                if (angular.isDefined($rootScope.containers) && oid == s.id && s.spa) {
                    e.pageIsHandling = true;
                    return;
                }

                // Redirect if not service portal? - chat with HR support does this...
                // And prevent endless redirect for requests like /sp/cache.do
                if (p.indexOf(".do") > 0 && p.indexOf("sp.do") == -1) {
                    var newUrl = $location.absUrl();
                    newUrl = newUrl.substr(newUrl.search(/[^\/]+.do/));
                    $window.location.href = "/" + newUrl;
                    return;
                }

                // Redirect if the current page is the login page
                if (!window.NOW.has_access && locationChanged) {
                    $window.location.href = $location.absUrl();
                    return;
                }

                oid = s.id;
                var t = getUrl();
                refreshPage(t);
            })

        });

})(angular);
