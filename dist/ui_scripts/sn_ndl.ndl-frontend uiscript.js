(function(angular) {
  angular.module('ndl-frontend', ['ui.router'])
    .provider('runtimeStates', function runtimeStates($stateProvider) {
      // runtime dependencies for the service can be injected here, at the provider.$get() function.
      this.$get = function($q, $timeout, $state) { // for example
        return {
          addState: function(name, state) {
            $stateProvider.state(name, state);
          }
        };
      };
    })

    .factory('urlMapping', function() {
      var dict = {};
      return {
        _get: function(key) {
          if (Object.keys(dict).indexOf(key) != -1)
            return dict[key];
          else
            return key;
        },
        _set: function(key, value) {
          dict[key] = value;
        },
        _setAll: function(obj) {
          dict = obj;
        }
      };
    })

    .service('dataService', function($http, $q) {

      var fetchRecord = function(table, sysId, fields) {

        var deferred = $q.defer();

        var url = '/api/snc/ndl_2_api/records?table=' + table + '&sys_id=' + sysId + '&fields=' + fields;

        var req = {
          method: 'GET',
          url: url,
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          }
        };

        $http(req)
          .success(function(data) {
            deferred.resolve(data);
          }).error(function(reason) {
            deferred.reject(reason);
          });
        return deferred.promise;
      };

      var fetchRecords = function(table, query, fields, order_by) {
        var deferred = $q.defer();
        var url;
        url = '/api/snc/ndl_2_api/records?table=' + table + '&query=' + query + '&fields=' + fields;
        if (order_by) {
          url = url + "&order_by=" + order_by;
        }
        var req = {
          method: 'GET',
          url: url,
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          }
        };

        $http(req)
          .success(function(response) {
            deferred.resolve(response);
          }).error(function(reason) {
            deferred.reject(reason);
          });
        return deferred.promise;
      };

      return {
        getWidgets: function() {
          return fetchRecords('solution_artifacts', 'active=true^type=widget^ORtype=OOB Widget^solution.u_common_name!=null', 'solution.name, solution, solution.u_common_name, u_widget_category, solution.sys_id');
        },

        getDetail: function(query, fields) {
          return fetchRecords('solution_artifacts', query, fields);
        },

        getSolutions: function(query, fields) {
          return fetchRecords('solutions', query, fields);
        },

        getAttachment: function(query, fields) {
          return fetchRecords('sys_attachment', query, fields);
        },

        getKnowledgeArticle: function(query, fields) {
          return fetchRecords('kb_knowledge', query, fields);
        },

        getExperiences: function() {
          return fetchRecords('solutions', "publishing_goal=Experience^state=3^u_quick_launch=true^u_show_on_siteLIKE811f5c631361fa002ea1b2566144b09a", 'name, sys_id');
        },

        getComponents: function() {
          return fetchRecords('solution_artifacts', 'type=Component', 'solution.name, solution, solution.u_common_name, u_widget_category, solution.sys_id, type, title, u_component_category', 'u_order');
        },

        getExperience: function(id) {
          return fetchRecord('solutions', id, 'name,sys_id');
        },

        getExperiencePages: function(id) {
          return fetchRecords('u_widget_pages', 'u_experience=' + id, 'sys_id, u_page_name, u_order,u_experience', 'u_order');
        },

        getExperiencePageComponents: function(id) {
          return fetchRecords('u_page_components', 'u_widget_page=' + id, 'sys_id, u_hotspot_name, u_show_hotspot, u_order, u_screenshot, u_solution_widget, u_solution_widget.sys_id, u_type', 'u_order');
        }

      };
    })

    .run(function($rootScope, $location, $window, $state, $timeout, $http, spUtil, glideUserSession, snRecordWatcher, urlMapping) {
      // Loop thru empting this out
      if ($rootScope.$$listenerCount['$locationChangeSuccess'] > 0) {
        $rootScope.$$listeners['$locationChangeSuccess'][0] = null;
      }

      var oldState = $location.search().state;
      var oid = $location.search().id;
      var oldPath = $location.path();

      $rootScope.$on('$locationChangeSuccess', function(e, newUrl, oldUrl) {
        var locationChanged = (oldUrl != newUrl);
        var s = $location.search();
        var p = $location.path();

        // Has portal changed?
        if (oldPath != p) {
          $window.location.href = $location.absUrl();
          return;
        }

        // page is handling?
        if (angular.isDefined($rootScope.containers) && oid == s.id && s.spa) {
          e.pageIsHandling = true;
          return;
        }

        // Redirect if not service portal? - chat with HR support does this...
        // And prevent endless redirect for requests like /sp/cache.do
        if (p.indexOf('.do') > 0 && p.indexOf('sp.do') == -1) {
          var newUrl = $location.absUrl();
          newUrl = newUrl.substr(newUrl.search(/[^\/]+.do/));
          $window.location.href = '/' + newUrl;
          return;
        }

        // Redirect if the current page is the login page
        if (!window.NOW.has_access && locationChanged) {
          $window.location.href = $location.absUrl();
          return;
        }

        if (s.state) {
          $state.go(urlMapping._get(s.state), s, {
            reload: urlMapping._get(s.state)
          });
          return;
        }

        if (oldPath == p) {
          $state.go(urlMapping._get('home'));
          return;
        }
        // TODO Apply the page api
        debugger;
      });
    })

    .run(function($http) {
      $http.defaults.headers.common['X-UserToken'] = window.g_ck;
    });

})(angular);