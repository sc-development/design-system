function WidgetDetailController($location, $rootScope, $state, $timeout, dataService) {
  var c = this;
  c.formatDate = formatDate;
  c.gaEvent = gaEvent;
  c.tabClicked = tabClicked;
  c.tabSelection = tabSelection;

  c.$onInit = function() {
    getWidgetInfo();
    getWidgetDocs();
    getWidgetUpdateSet();
  };

  function activateClipboard() {
    var clipboard = new Clipboard('.btn');
    clipboard.on('success', function (e) {
      e.clearSelection();
    });
  }

  function activateHighlights() {
    return $timeout(function() {
      angular.element('pre').each(function (i, block) {
        hljs.highlightBlock(block);
      });
    }, 0);
  }

  function formatDate(dateString) {
    return moment(dateString).format('MM.DD.YYYY');
  }

  function gaEvent(event) {
    ga('send', 'event', 'widget-action', event);
  }

  function getTitle(docs) {
    return docs['solution.u_common_name'] ? docs['solution.u_common_name'] : '';
  }

  function getWidgetDocs() {
    var fields = 'link, u_body_html_template, u_demo_data, solution.u_common_name';
    var query = 'type=Documentation^solution=' + $state.params.sys_id;
    return dataService.getDetail(query, fields).then(function(res) {
      var docs = res.result.length ? res.result[0] : null;
      if (docs) {
        c.docTitle = getTitle(docs);
        c.gitLink = setGitLink(docs.link);
        c.readme = setReadme(docs);
      }
    });
  }

  function getWidgetInfo() {
    var fields = 'type, solution.u_common_name, u_body_html_template, u_client_script, u_css, u_server_script, u_demo_data, u_link_script, u_option_schema, u_timestamp, sys_updated_on, solution.u_contributors';
    var query = 'type=widget^ORtype=OOB Widget^ORtype=Component^solution=' + $state.params.sys_id;
    return dataService.getDetail(query, fields).then(function(res) {
      c.widgetInfo = res.result.length ? res.result[0] : null;
    }).then(function() {
      setDefaults();
      activateHighlights();
      activateClipboard();
      setTracking();
    });
  }

  function getWidgetUpdateSet() {
    var field = 'link';
    var query = 'type=Update Set^solution=' + $state.params.sys_id;
    return dataService.getDetail(query, field).then(function(res) {
      c.widgetUpdateSet = res.result.length ? res.result[0] : null;
    });
  }

  function setDefaults() {
    $rootScope.itemSelected = null;
    c.selectedCode = 'html';
  }

  function setGitLink(url) {
    return [url].map(trimLink).toString();
  }

  function setReadme(md) {
    return [md].map(trimReadme).toString();
  }

  function setTracking() {
    var widgetName = c.widgetInfo && c.widgetInfo['solution.u_common_name'] ? c.widgetInfo['solution.u_common_name'] : '';
    ga('set', 'page', 'widget-detail');
    ga('set', 'contentGroup1', widgetName);
    var exp = $location.search().exp;
    if (!exp) {
      ga('set', 'contentGroup2', null);
    }
    ga('send', 'pageview');
  }

  function tabClicked(tabSelected) {
    ga('send', 'event', 'widget-view', tabSelected);
  }

  function tabSelection(tabSelected) {
    c.selectedTab = tabSelected;
    ga('send', 'event', 'widget-view', tabSelected);
  }

  function trimLink(link) {
    var index = link.search(new RegExp('/readme', 'i'));
    return link.substring(0, index);
  }

  function trimReadme(md) {
    var description = 'user-content-description';
    var template = md.u_body_html_template;
    var subTemplate = template.substring(template.indexOf(description) - 12, template.length);
    return template && template.includes(description) ? subTemplate : template;
  }
}