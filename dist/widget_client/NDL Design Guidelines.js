function DesignGuidelinesController($filter, $state, dataService) {
  var c = this;
  c.sendGAEvent = sendGAEvent;

  c.$onInit = function () {
    var solution = $state.params.sys_id;
    c.articles = [];
    getKnowledgeArticles(solution);
    ga('set', 'page', 'design-guidelines');
    ga('set', 'contentGroup2', null);
    ga('set', 'contentGroup1', null);
  };

  function getKnowledgeArticles(solution) {
    var fields = 'u_order, u_knowledge_article.short_description, u_knowledge_article.sys_id, u_knowledge_article.text, u_knowledge_article.number';
    return dataService
      .getDetail('solution=' + solution, fields)
      .then(function (response) {
        sendInitialGAData(response.result);
        $filter('orderBy')(response.result, 'u_order').forEach(function (article) {
          buildArticles(article);
        });
        sendInitialGAData();
      });
  }

  function buildArticles(kbObject) {
    var article = {
      title: kbObject['u_knowledge_article.short_description'],
      text: kbObject['u_knowledge_article.text'],
      number: kbObject['u_knowledge_article.number'],
      sys_id: kbObject['u_knowledge_article.sys_id']
    };

    var fields = 'file_name, sys_id, content_type';
    // if (article.title !== 'Overview') {
    // first article displayed at top of page and includes attachments
    // any others shown in tabs
    dataService
      .getAttachment('table_sys_id=' + article.sys_id, fields)
      .then(function (response) {
        // allow for 1 Sketch file, 1 import set
        if (response.result && response.result.length) {
          response.result.forEach(function (attachment) {
            if (attachment.content_type == 'application/xml') {
              article.xml_file = {
                name: attachment.file_name,
                sys_id: attachment.sys_id
              }
            } else if (attachment.content_type == 'application/octet-stream') {
              // that's a Sketch file
              article.sketch_file = {
                name: attachment.file_name,
                sys_id: attachment.sys_id
              }
            }
          });
        }
      });
    // }
    c.articles.push(article);
  }

  function sendInitialGAData() {
    if (!c.articles.length)
      return;
    ga('set', 'contentGroup1', c.articles[0].title);
    ga('send', 'pageview');
  }

  function sendGAEvent(event) {
    ga('send', 'event', 'guidelines-action', event);
  }
}