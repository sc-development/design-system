function SideNavController($filter, $location, $q, $rootScope, $scope, $state, $timeout, $window, dataService) {
  var c = this;
  c.openGuidelines = openGuidelines;
  c.openComponents = openComponents;
  c.openWidgets = openWidgets;
  c.openApplications = openApplications;
  c.goToComponent = goToComponent;
  c.goToState = goToState;
  c.querySearch = querySearch;
  c.selectCategory = selectCategory;

  c.$onInit = function() {
    fetchNavigation();
    setOverviewDefaults();
  };

  function fetchNavigation() {
    var promises = [getDesignGuidelines(), getComponents(), getWidgets(), getApplications()];
    $q.all(promises).then(function() {
      setSelectedState();
      updateNavState();
      setAnchorSelection();
    });
  }

  function setSelectedState() {
    var sysId = $state.params.sys_id;
    if (sysId) {
      var selectedWidget = $filter('filter')(c.widgetsListOriginal, {'solution.sys_id': sysId});
      selectedWidget.forEach(function(item) {
        if (item.u_widget_category !== null) {
          c.widgetsExpanded = true;
          c.selectedCategory = item.u_widget_category;
          c.selectedItem = sysId;
        }
      });
      var selectedComponent = $filter('filter')(c.components, {'solution.sys_id': sysId});
      selectedComponent.forEach(function(item) {
        if (item.u_component_category !== null) {
          c.componentsExpanded = true;
          c.selectedItem = sysId;
          c.selectedCategory = item.u_component_category;
        }
      });
      var selectedGuideline = $filter('filter')(c.designGuidelines, {'sys_id': sysId});
      selectedGuideline.forEach(function() {
        c.guidelinesExpanded = true;
        c.selectedItem = sysId;
      });
      c.selectedAnchor = $state.params.anchor;
    }
    var exampleId = $state.params.exp;
    if (exampleId) {
      $filter('filter')(c.experiences, {'sys_id': exampleId}).forEach(function(item) {
        c.selectedItem = item.sys_id;
        c.examplesExpanded = true;
      });
    }
  }

  function setOverviewDefaults() {
    if ($state.params.design) {
      c.guidelinesExpanded = true;
      c.selectedItem = '5212a76b130303402ea1b2566144b00e';
    }
    if ($state.params.unit) {
      c.componentsExpanded = true;
      c.selectedCategory = 'unit-overview';
      c.selectedItem = null;
    }
    if ($state.params.featured) {
      c.widgetsExpanded = true;
      c.selectedCategory = 'widget-overview';
      c.selectedItem = null;
    }
    if ($state.params.app) {
      c.examplesExpanded = true;
      c.selectedCategory = 'app-overview';
      c.selectedItem = null;
    }
  }

  function getDesignGuidelines() {
    var fields = 'sys_id, number, name, order';
    return dataService.getSolutions('type=7a68f8381307c3002ea1b2566144b0f1', fields).then(function(response) {
      c.designGuidelines = response.result;
    });
  }

  function getWidgets() {
    return dataService.getWidgets().then(function(response) {
      c.widgetsList = response.result;
      c.widgetsListOriginal = c.widgetsList;
      filterWidgets();
    });
  }

  function formatWidgetCategories(resultList) {
    var widgetObj = {};
    resultList.forEach(function (item) {
      if (Object.keys(widgetObj).indexOf(item.u_widget_category) === -1) {
        widgetObj[item.u_widget_category] = [];
      }
      widgetObj[item.u_widget_category].push(item);
    });
    c.widgetObj = sortObject(widgetObj);
  }

  function slugify(text) {
    return text.toString().toLowerCase()
      .replace(/\s+/g, '-')           // Replace spaces with -
      .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
      .replace(/\-\-+/g, '-')         // Replace multiple - with single -
      .replace(/^-+/, '')             // Trim - from start of text
      .replace(/-+$/, '');            // Trim - from end of text
  }

  function formatComponentCategories(resultList) {
    var tempObj = {};
    resultList.forEach(function (item) {
      if (Object.keys(tempObj).indexOf(item.u_component_category) == -1) {
        tempObj[item.u_component_category] = [];
      }
      item.slug = slugify(item.title);
      tempObj[item.u_component_category].push(item);
    });
    return sortObject(tempObj);
  }

  function selectCategory(key) {
    if (c.selectedCategory === key) {
      c.selectedCategory = null;
    } else {
      c.selectedCategory = key;
    }
  }

  function getComponents() {
    return dataService.getComponents().then(function(response) {
      c.components = c.initialComponents = response.result;
      c.componentsObj = formatComponentCategories(c.components);
    });
  }

  function getApplications() {
    return dataService.getExperiences().then(function(response) {
      c.experiences = c.experiencesOriginal = response.result;
    });
  }

  function filterWidgets() {
    var widgetList = c.widgetsListOriginal;
    c.components = c.initialComponents;
    if (typeof(c.initialWidgetsExpanded) != 'undefined') {
      c.widgetsExpanded = c.initialWidgetsExpanded;
    }
    if (typeof(c.initialComponentsExpanded) != 'undefined') {
      c.componentsExpanded = c.initialComponentsExpanded;
    }
    if (typeof(c.initialExpandExperiences) != 'undefined') {
      c.examplesExpanded = c.initialExpandExperiences;
    }
    formatWidgetCategories(widgetList);
  }

  function sortObject(obj) {
    var arr = [];
    var prop;
    for (prop in obj) {
      if (obj.hasOwnProperty(prop)) {
        arr.push({
          'key': prop,
          'value': obj[prop]
        });
      }
    }
    arr = sortByKey(arr, 'key');
    var newObj = {};
    for (var el in arr) {
      newObj[arr[el].key] = sortByKey(arr[el].value, 'solution.u_common_name');
    }
    arr = newObj;
    return arr;
  }

  function sortByKey(array, key) {
    return array.sort(function (a, b) {
      var x = a[key];
      var y = b[key];
      return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
  }

  function openGuidelines() {
    c.guidelinesExpanded = !c.guidelinesExpanded;
    c.componentsExpanded = false;
    c.widgetsExpanded = false;
    c.examplesExpanded = false;
    c.selectedItem = '5212a76b130303402ea1b2566144b00e';
    if ($rootScope.isHomepage !== true) {
      $location.url('?state=design-guidelines&sys_id=5212a76b130303402ea1b2566144b00e&design=true');
    }
  }

  function openComponents() {
    c.componentsExpanded = !c.componentsExpanded;
    c.widgetsExpanded = false;
    c.examplesExpanded = false;
    c.guidelinesExpanded = false;
    c.selectedCategory = 'unit-overview';
    if ($rootScope.isHomepage !== true) {
      $location.url('?state=featured-components&unit=true');
    }
  }

  function openWidgets() {
    c.widgetsExpanded = !c.widgetsExpanded;
    c.componentsExpanded = false;
    c.examplesExpanded = false;
    c.guidelinesExpanded = false;
    c.selectedCategory = 'widget-overview';
    if ($rootScope.isHomepage !== true) {
      $location.url('?state=featured-widgets&featured=true');
    }
  }

  function openApplications() {
    c.examplesExpanded = !c.examplesExpanded;
    c.componentsExpanded = false;
    c.widgetsExpanded = false;
    c.guidelinesExpanded = false;
    c.selectedCategory = 'app-overview';
    c.selectedItem = null;
    if ($rootScope.isHomepage !== true) {
      $location.url('?state=featured-applications&app=true');
    }
  }

  function updateNavState() {
    $scope.$on('updateNav', function(e, params) {
      if (params.design) {
        openGuidelines();
        c.guidelinesExpanded = true;
      } else if (params.unit) {
        openComponents();
        c.componentsExpanded = true;
      } else if (params.featured) {
        openWidgets();
        c.widgetsExpanded = true;
      } else if (params.app) {
        openApplications();
        c.examplesExpanded = true;
      }
    });
  }

  function getCategoryType(query, list, type) {
    var category;
    var categories = {
      'app': function() {
        category = $filter('filter')(list, {'name': query});
      },
      'design': function() {
        category = $filter('filter')(list, {'name': query});
      },
      'comp': function() {
        category = $filter('filter')(list, {'solution.u_common_name': query});
      },
      'widget': function() {
        category = $filter('filter')(list, {'solution.u_common_name': query});
      }
    };
    (categories[type] || categories['default'])();
    return category;
  }

  function querySearch(query, list, type) {
    var category = getCategoryType(query, list, type);
    return category;
  }

  function goToState(sysId, type) {
    var link;
    if (type === 'design') {
      link = '?state=design-guidelines&sys_id=';
    } else if (type === 'app') {
      link = '?state=applications&exp=';
    } else {
      link = '?state=widget-detail&sys_id=';
    }
    return $location.url(link + sysId);
  }

  function goToAnchor(anchor) {
    $timeout(function() {
      $window.location.href = '#user-content-' + anchor;
    }, 500);
  }

  function goToComponent(item, anchor) {
    c.selectedAnchor = anchor;
    c.selectedItem = item;
    $location.url('?state=widget-detail&sys_id=' + item + '&anchor=' + anchor + '#user-content-' + anchor);
    goToAnchor(anchor);
  }

  function setAnchorSelection() {
    var anchor = $location.search().anchor;
    if (anchor) {
      goToAnchor(anchor);
    }
  }
}