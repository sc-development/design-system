function ExamplesController($filter, $q, $state, dataService) {
  var c = this;
  c.getSelectedWidget = getSelectedWidget;
  c.getSysId = getSysId;
  c.goToNext = goToNext;
  c.goToPage = goToPage;
  c.goToPrev = goToPrev;

  c.$onInit = function() {
    activateExamples();
  };

  function activateExamples() {
    getExamples();
    if ($state.params.index) {
      c.selectedWidget = $state.params.index;
    }
    if (c.data.expObj && c.data.experienceId === $state.params.exp.sys_id) {
      c.expObj = c.data.expObj;
      c.pages = c.data.pages;
    } else {
      c.data.pageSelected = 0;
      buildExamples().then(function() {
        c.data.expObj = c.expObj;
      });
    }
  }

  function getExamples() {
    if (c.data.experience) {
      if (c.data.experience.sys_id === $state.params.exp.sys_id) {
        c.example = c.data.experience;
        return;
      }
    }
    return dataService.getExperience($state.params.exp).then(function(response) {
      c.example = c.data.experience = response.result;
      c.data.experienceId = c.data.experience.sys_id;
      setTracking();
    });
  }

  function buildExamples() {
    c.expObj = [];
    var listOfPromises = [];
    var deferred = $q.defer();
    dataService.getExperiencePages($state.params.exp).then(function(response) {
      c.pages = c.data.pages = $filter('orderBy')(response.result, 'u_order');
    }).then(function() {
      angular.forEach(c.pages, function(item, key) {
        listOfPromises.push(
          dataService.getExperiencePageComponents(c.pages[key].sys_id).then(function(response) {
            c.expObj.push({
              order: c.pages[key].u_order,
              pageName: c.pages[key].u_page_name,
              widgets: response.result,
              widgetCount: setWidgetCount(response.result)
            });
          })
        );
      });
      $q.all(listOfPromises).then(function(response) {
        c.expObj = $filter('orderBy')(c.expObj, 'order');
        deferred.resolve(response);
      });
    });
    return deferred.promise;
  }

  function getSysId(widgetLink) {
    if (widgetLink && widgetLink.length > 1) {
      var sysId = widgetLink.slice(-32);
      return sysId;
    }
  }

  function getSelectedWidget(widgetIndex, sysId) {
    c.selectedWidget = widgetIndex;
    if ($state.params.sys_id !== sysId) {
      $state.go('ndl.exp-detail', {
        exp: c.data.experience,
        sys_id: sysId,
        index: widgetIndex
      }, {
        reload: false
      });
    }
  }

  function goToNext() {
    c.data.pageSelected += 1;
    c.selectedWidget = null;
  }

  function goToPage(index) {
    c.data.pageSelected = index;
    c.selectedWidget = null;
  }

  function goToPrev() {
    c.data.pageSelected -= 1;
    c.selectedWidget = null;
  }

  function setTracking() {
    if (c.example.name) {
      ga('set', 'page', 'experience-detail');
      ga('set', 'contentGroup2', c.example.name);
      ga('send', 'pageview');
    }
  }

  function setWidgetCount(widgetObj) {
    var widgetCount = widgetObj.filter(function(widget) {
      return widget.u_show_hotspot === 'true';
    });
    return widgetCount.length;
  }
}