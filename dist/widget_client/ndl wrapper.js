function WrapperController($location, $rootScope, $state, runtimeStates, spUtil, urlMapping) {
  var c = this;
  c.floatMenu = false;

  c.$onInit = function () {
    setRouting();
    activateSubHeader();
  };

  function activateSubHeader() {
    spUtil.get('ndl-sub-header').then(function (response) {
      $rootScope.subpageWidget = response;
    });
  }

  function setRouting() {
    urlMapping._setAll({
      'home': 'ndl.home',
      'widget-detail': 'ndl.widget-detail',
      'applications': 'ndl.applications',
      'exp-detail': 'ndl.exp-detail',
      'design-guidelines': 'ndl.design-guidelines',
      'featured-components': 'ndl.featured-components',
      'featured-widgets': 'ndl.featured-widgets',
      'featured-applications': 'ndl.featured-applications'
    });

    runtimeStates.addState('ndl', {
      params: {},
      templateUrl: 'ndl'
    });

    runtimeStates.addState('ndl.home', {
      params: {},
      templateUrl: 'ndl-home',
      onEnter: function () {
        $rootScope.hideLeftNav = true;
        $rootScope.isHomepage = true;
      },
      onExit: function () {
        $rootScope.hideLeftNav = false;
        $rootScope.isHomepage = false;
      }
    });

    runtimeStates.addState('ndl.widget-detail', {
      params: {
        sys_id: null,
        anchor: null,
        unit: null,
        featured: null
      },
      templateUrl: 'ndl-widget-detail'
    });

    runtimeStates.addState('ndl.applications', {
      params: {
        exp: null,
        app: null
      },
      templateUrl: 'ndl-exp-home',
      onEnter: function () {
        $rootScope.fixedFooter = true;
      },
      onExit: function () {
        $rootScope.fixedFooter = false;
      }
    });

    runtimeStates.addState('ndl.exp-detail', {
      params: {
        sys_id: null,
        exp: null,
        index: null
      },
      templateUrl: 'ndl-exp-detail',
      onEnter: function () {
        $rootScope.fixedFooter = true;
      },
      onExit: function () {
        $rootScope.fixedFooter = false;
      }
    });

    runtimeStates.addState('ndl.design-guidelines', {
      params: {
        sys_id: null,
        design: null
      },
      templateUrl: 'ndl-design-guidelines'
    });

    runtimeStates.addState('ndl.featured-components', {
      params: {
        sys_id: null,
        unit: null
      },
      templateUrl: 'ndl-featured-components'
    });

    runtimeStates.addState('ndl.featured-widgets', {
      params: {
        sys_id: null,
        featured: null
      },
      templateUrl: 'ndl-featured-widgets'
    });

    runtimeStates.addState('ndl.featured-applications', {
      params: {
        sys_id: null,
        app: null
      },
      templateUrl: 'ndl-featured-applications'
    });

    var params = $location.search();
    var newState = $location.search().state;

    // try and catch in case viewing on $spd.do
    try {
      if (!newState) {
        $state.go(urlMapping._get('home'));
      } else {
        $state.go(urlMapping._get(newState), params, {
          refresh: true
        });
      }
    } catch (e) {
    }
  }
}