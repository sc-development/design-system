function peHeader($rootScope, $scope, snRecordWatcher, spUtil, $window, $location, $uibModal, cabrillo, $timeout) {
  var c = this;
  c.selectCategory = selectCategory;

  c.expandMenu = false;
  c.chevronM = angular.element(document.querySelector('.desktop-chevron'));
  c.chevronD = angular.element(document.querySelector('.mobile-chevron'));

  $scope.userID = $scope.user.sys_id;

  if (cabrillo.isNative())
    $scope.isViewNative = true;

  $scope.isHomepage = function() {
    if (!$scope.page.id)
      return true;

    if ($scope.page.id == $scope.portal.homepage_dv)
      return true;

    return false;
  };

  $rootScope.$on('feedbackExists', function() {
    //$rootScope.$broadcast('fbStyleButton','hover');
  });

  c.openFeedback = function() {
    $rootScope.$broadcast('openFeedback', '');
    c.expandPageMenu(null, false);
  };

  c.goItem = function(item) {
    if (item.condition === 'feedback') {
      c.openFeedback();
    } else {
      $window.location = item.url;
    }
  };
  c.expandPageMenu = function(evt, forced) {
    if (evt) {
      evt.stopPropagation();
      var el = angular.element(evt.target);

      c.expandMenu = !c.expandMenu;
      if (el[0].id === 'expandPage') {
        c.chevronM.triggerHandler('click');
        c.chevronD.triggerHandler('click');
      }
    }

    if (forced === false) {
      c.chevronM.triggerHandler('click');
      c.chevronD.triggerHandler('click');
      c.expandMenu = forced;
    }

  };

  c.$onInit = function() {
    Fay.init();
  };

  function selectCategory(key) {
    if (c.selectedCategory == key) {
      c.selectedCategory = null;
    } else {
      c.selectedCategory = key;
    }
  }
}