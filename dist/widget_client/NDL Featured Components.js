function FeaturedComponentsController() {
  var c = this;

  c.$onInit = function() {
    setTracking();
  };

  function setTracking() {
    ga('set', 'page', 'widget-detail');
    ga('set', 'contentGroup1', 'Components Overview');
    ga('set', 'contentGroup2', null);
    ga('send', 'pageview');
  }
}
