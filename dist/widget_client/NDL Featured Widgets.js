function FeaturedWidgetsController(dataService) {
  var c = this;
  c.formatDate = formatDate;
  c.tabClicked = tabClicked;

  c.$onInit = function() {
    getFeaturedWidgets();
    getLastUpdatedWidgets();
    setTracking();
  };

  function formatDate(dateString) {
    return moment(dateString).format('MM.DD.YYYY');
  }

  function getFeaturedWidgets() {
    var fields = 'solution.u_common_name, solution.order, solution.sys_id, solution.details, screenshot, sys_updated_on, u_timestamp';
    var query = 'type=widget^solution.featured=true^solution.order!=null';
    return dataService.getDetail(query, fields).then(function(res) {
      c.featuredWidgets = res.result.length ? res.result.map(setUpdated).filter(isScreenshot) : [];
    });
  }

  function getLastUpdatedWidgets() {
    var fields = 'solution.u_common_name, solution.sys_id, solution.details, screenshot, sys_updated_on, u_timestamp';
    var query = 'type=widget^solution.u_common_name!=null^solution.details!=null^ORDERBYDESCu_timestamp';
    return dataService.getDetail(query, fields).then(function(res) {
      c.updatedWidgetList = res.result.length ? getUnique(res.result, 'solution.sys_id').map(setUpdated).filter(isScreenshot) : [];
    });
  }

  function getUnique(arr, param) {
    return arr.filter(function(item, pos, array) {
      return array.map(function(mapItem) {
        return mapItem[param];
      }).indexOf(item[param]) === pos;
    });
  }

  function isScreenshot(list) {
    return list.screenshot !== null;
  }

  function setUpdated(res) {
    return {
      sysId: res['solution.sys_id'],
      name: res['solution.u_common_name'],
      details: res['solution.details'],
      order: res['solution.order'],
      screenshot: res.screenshot,
      updateTime: res.u_timestamp || res.sys_updated_on
    };
  }

  function setTracking() {
    ga('set', 'page', 'widget-detail');
    ga('set', 'contentGroup1', 'Widgets Overview');
    ga('set', 'contentGroup2', null);
    ga('send', 'pageview');
  }

  function tabClicked(tabValue) {
    ga('send', 'event', 'widgets-overview-tab-clicked', tabValue);
  }
}