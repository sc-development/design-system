function addBookmark($rootScope, $scope, $window, $timeout) {
	/* widget controller */
	var c = this;
	c.ultimateHide = false;

	var userCookies = document.cookie.split(";");

	var myCookie = '';
	for(var i=0; i<userCookies.length; i++){
		var name = userCookies[i].split('=')[0];
		var value = userCookies[i].split('=')[1];
		if(name.trim() === $rootScope.portal.sys_id.toString()){
			if(value){
				myCookie = decodeURI(unescape(value));
			}else{
				document.cookie = $rootScope.portal.sys_id.toString()+"=;expires=Thu, 01 Jan 1970 00:00:01 GMT;";
			}
		}
	}
	
	if(myCookie.length === 0){
		document.cookie = $rootScope.portal.sys_id.toString()+"="+escape(encodeURI("visits=1")) + "; expires=Wed, 01 Jan 2025 00:00:00 UTC; path=/";
	}else{
		switch(myCookie.split("=")[1]){
			case "1":
					document.cookie = $rootScope.portal.sys_id.toString()+"="+escape(encodeURI("visits=2")) + "; expires=Wed, 01 Jan 2025 00:00:00 UTC; path=/";
					break;
			case "2":
					document.cookie = $rootScope.portal.sys_id.toString()+"="+escape(encodeURI("visits=3")) + "; expires=Wed, 01 Jan 2025 00:00:00 UTC; path=/";
					break;
			default:
					c.ultimateHide = true;
		}
	}
	
	c.userAgent = $window.navigator.userAgent;
	var ua = detect.parse(c.userAgent);
	
	c.abHide = false;
	c.goTop = false;
	c.goBottom = false;
	c.showIOS = false;
	c.showChrome = false;
	c.showSafari = false;
	c.showOther = false;
	c.force = getParameterValue('bookmark');
	
	if($rootScope.portal.icon===''){
		c.imageSrc = 'native_notification_icon.png';
	}else{
		c.imageSrc = $rootScope.portal.icon;
	}
	
	c.device = {
		os : ua.os.family.toUpperCase(),
		browser : ua.browser.family.toUpperCase(),
		type : ua.device.type.toUpperCase()
	};
	
	$rootScope.deviceInfo = {
		os : ua.os.family.toUpperCase(),
		browser : ua.browser.family.toUpperCase(),
		device : ua.device.type.toUpperCase(),
		user_agent: c.userAgent
	};
	
	$rootScope.$on('fbStyleBookmark', function(evt,objStyle) {
		if(objStyle){
			$rootScope.bmArrow = objStyle.arrow;
			$rootScope.bmContent = objStyle.content;
		}
	});
	
	/*
	//Use broadcast as follows 
	$rootScope.$broadcast('fbStyleBookmark',{
		"arrow" : { "background-color": "#09639b",
								"border-color": "#09639b"
								},
		"content" : { "background-color": "#09639b" }
	});
	*/
	
	/* Scope Functions */
	$scope.initializeBookMark = function() {
		$rootScope.$broadcast('AddBookmarkExists');
		if(c.force){
			forceMode(c.force);
		}else{
			autoDetect();
		}
	};
	
	/* Let's Go */
	$scope.initializeBookMark();
		
	function forceMode(force){
		c.ultimateHide = false;
		c.instructions = "Add this web app to your bookmarks by ";
		switch(force.toLowerCase()){
			case 'ios':
				c.instructions = "Add this web app to your homescreen.";
				c.showIOS = true;
				c.goBottom = true;
				break;
			case 'chrome':
				c.showChrome = true;
				c.goTop = true;
				break;
			case 'safari':
				c.showSafari = true;
				c.goTop = true;
				break;
			default:
				c.showOther = true;
				c.goTop = true;
		}
	}
	
	function autoDetect(){
		c.instructions = "Add this web app to your bookmarks by ";
		if(c.device.type =="MOBILE"){
			c.showChromeD = false;
			if((c.device.os.indexOf('IOS') != -1) && (c.device.browser.indexOf("SAFARI") != -1 )) {
				c.instructions = "Add this web app to your homescreen.";
				c.showIOS = true;
				c.goBottom = true;
			}else{
				if(c.device.browser.indexOf("CHROME") != -1 ) {
					c.showChrome = true;
					c.goTop = true;
				}else{
					c.showOther = true;
					c.goTop = true;
				}
			}
		}else{
			if(c.device.browser.indexOf("SAFARI") != -1 ) {
				c.showSafari = true;
				c.goTop = true;
			}else{
				if(c.device.browser.indexOf("CHROME") != -1 ) {
					c.showChrome = true;
					c.goTop = true;
				}else{
					c.showOther = true;
					c.goTop = true;
				}
			}
		}
		$timeout( function(){
			c.abHide = true;
		}, c.data.options.hide_duration );
	}
	
	function getParameterValue(name) {  
		name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");  
		var regexS = "[\\?&]" + name + "=([^&#]*)";  
		var regex = new RegExp(regexS);  
		var results = regex.exec(top.location);  
		if (results == null) {  
			return "";  
		} else {  
			return unescape(results[1]);  
		}  
	}
}