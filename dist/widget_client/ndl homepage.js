function HomepageController($location, $rootScope) {
  var c = this;
  c.go = go;

  c.$onInit = function () {
    $location.search({});
    ga('set', 'page', 'homepage');
    ga('send', 'pageview');
  };

  function go(state, category, id, url) {
    var params = {};
    params.state = state;
    params[category] = 'true';
    if (id) {
      params.sys_id = id;
    }
    $rootScope.$broadcast('updateNav', params);
    goToUrl(url);
  }

  function goToUrl(url) {
    return $location.url(url);
  }
}
