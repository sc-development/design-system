function AnalyticsController($scope) {
  var c = this;
  var instanceName = window.location.hostname.split('.')[0];
  if (!$scope.portal) {
    return;
  }

  var portal = $scope.portal.url_suffix;
  var gaURL;

  if (c.options.debug == 'true') {
    gaURL = 'https://www.google-analytics.com/analytics_debug.js';
  } else {
    gaURL = 'https://www.google-analytics.com/analytics.js';
  }
  /* HotJar */
  (function (h, o, t, j, a, r) {
    h.hj = h.hj || function () {
      (h.hj.q = h.hj.q || []).push(arguments);
    };
    if ((instanceName == 'scdevelopment') && (portal == 'snds')) {
      h._hjSettings = {hjid: 617673, hjsv: 5};
    } else if ((instanceName == 'sc') && (portal == 'snds')) {
      h._hjSettings = {hjid: 617676, hjsv: 5};
    } else {
      return;
    }
    a = o.getElementsByTagName('head')[0];
    r = o.createElement('script');
    r.async = 1;
    r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
    a.appendChild(r);
  })(window, document, '//static.hotjar.com/c/hotjar-', '.js?sv=');

  (function (i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r;
    i[r] = i[r] || function () {
      (i[r].q = i[r].q || []).push(arguments);
    }, i[r].l = 1 * new Date();
    a = s.createElement(o), m = s.getElementsByTagName(o)[0];
    a.async = 1;
    a.src = g;
    m.parentNode.insertBefore(a, m);
  })(window, document, 'script', gaURL, 'ga');

  ga('create', {
    trackingId: c.options.gacode,
    cookieDomain: 'auto',
    userId: $scope.user.user_name
  });
}