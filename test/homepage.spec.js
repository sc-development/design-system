describe('Homepage controller', function() {

  it('should have a named function', function() {
    expect(HomepageController).toBeDefined();
  });

  it('has three arguments', function() {
    var argLength = HomepageController.length;
    expect(argLength).toEqual(2);
  });
});